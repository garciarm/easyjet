#include "../VBSJetsSelectorAlg.h"
#include "../BaselineVarsFullLepAlg.h"
#include "../BaselineVarsSemiLepAlg.h"
#include "../BaselineVarsFullHadAlg.h"
#include "../FullLepSelectorAlg.h"
#include "../SemiLepSelectorAlg.h"
#include "../FullHadSelectorAlg.h"
using namespace VBSHIGGS;

DECLARE_COMPONENT(VBSJetsSelectorAlg)
DECLARE_COMPONENT(BaselineVarsFullLepAlg)
DECLARE_COMPONENT(BaselineVarsSemiLepAlg)
DECLARE_COMPONENT(BaselineVarsFullHadAlg)
DECLARE_COMPONENT(FullLepSelectorAlg)
DECLARE_COMPONENT(SemiLepSelectorAlg)
DECLARE_COMPONENT(FullHadSelectorAlg)